import axios from "axios";

const axiosPosts = axios.create({
    baseURL: 'https://blog-2-5ee48-default-rtdb.firebaseio.com/'
});

// axiosPosts.interceptors.request.use(req => {
//     console.log('[In request interceptors]', req);
//     return req;
// });
//
// axiosPosts.interceptors.response.use(res => {
//     console.log('[In response interceptors]', res);
//     return res;
// }, err => {
//     console.log('[In error response interceptor]', err);
//     throw err;
// });

export default axiosPosts;