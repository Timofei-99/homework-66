import React from 'react';

const Post = (props) => {
    return (
        <div className='border p-3 mb-3'>
            <p>Created on: {props.date}</p>
            <h3>{props.title}</h3>
            <button className='btn btn-primary' onClick={props.info}>Read more >></button>
        </div>
    );
};

export default Post;