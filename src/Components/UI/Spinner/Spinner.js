import React from 'react';
import './Spinner.css';
import Backdrop from "../Backdrop/Backdrop";

const Spinner = (props) => {
    return (
        <>
            <Backdrop
                show={props.show}
            />
            <div className="Spinner"
                 style={{
                     display: props.show ? 'block' : 'none',
                     opacity: props.show ? '1' : '0',
                 }}>Loading...
            </div>
        </>

    )

};

export default Spinner;