import React, {useEffect, useState} from 'react';
import Post from "../../Components/Post/Post";
import axiosPosts from "../../axiosPosts";
import errorHandler from "../../hoc/errorHandler/errorHandler";

const MainPage = ({history}) => {
    const [post, setPost] = useState({});

    useEffect(() => {
        const getPost = async () => {
            const response = await axiosPosts.get('/posts.json');
            const posts = response.data;
            setPost(posts);
        }

        getPost().catch(console.error);
    }, []);

    let result = Object.keys(post).map(p => {
        return (
            <Post
                key={p}
                title={post[p].title}
                date={post[p].date}
            />
        )
    }).reverse();




    return (
        <div className='d-flex flex-column pt-5'>
            {result}
        </div>
    );
};

export default errorHandler(MainPage, axiosPosts);

