import React, {useEffect, useMemo, useState} from 'react';
import Spinner from "../../Components/UI/Spinner/Spinner";
import Modal from "../../Components/UI/Modal/Modal";

const errorHandler = (WrappedComponent, axios) => {
    return function ErrorHandlerHOC(props) {
        const [error, setError] = useState(null);
        const [loading, setLoading] = useState(true);

        const reqIcId = useMemo(() => {
            axios.interceptors.request.use(
                req => {
                    console.log('req');
                    setLoading(true);
                    return req;
                },
                error => {
                    setLoading(false);
                    setError(error);
                    throw error;
                }
            )
        }, []);

        const resIcId = useMemo(() => {
            axios.interceptors.response.use(
                res => {
                    console.log('res');
                    setLoading(false);
                    return res;
                },
                error => {
                    setLoading(false);
                    setError(error);
                    console.log('[In HOC interceptor]');
                    throw error;
                }
            );
        }, []);

        useEffect(() => {
            return () => {
                axios.interceptors.request.eject(reqIcId);
                axios.interceptors.response.eject(resIcId);
            }
        }, [reqIcId, resIcId]);

        return (
            <>
                <Spinner show={loading}/>
                <Modal show={Boolean(error)}>
                    {error && error.message}
                </Modal>
                <WrappedComponent {...props}/>

            </>
        )
    };
};

export default errorHandler;